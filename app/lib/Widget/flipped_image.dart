import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:math' as math;

class FlippedImage extends StatefulWidget {
  FlippedImage({Key key, this.opacity, this.link}) : super(key: key);

  @required
  final double opacity;
  @required
  final String link;

  @override
  _FlippedImageState createState() => _FlippedImageState();
}

class _FlippedImageState extends State<FlippedImage> {
  double _flip;

  @override
  void initState() {
    _flip = 0.0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 5,
        child: AnimatedOpacity(
          opacity: widget.opacity,
          duration: Duration(milliseconds: 700),
          child: Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(_flip),
            child: Container(
              alignment: FractionalOffset.center,
              child: GestureDetector(
                  child: Container(
                    child: CachedNetworkImage(
                      imageUrl: widget.link,
                      errorWidget: (context, url, error) => Container(
                        height: 0,
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      if (_flip == 0.0)
                        _flip = math.pi;
                      else
                        _flip = 0.0;
                    });
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
