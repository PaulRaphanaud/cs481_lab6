import 'package:app/Networking/APIRequest.dart';
import 'package:app/Widget/flipped_image.dart';
import 'package:flutter/material.dart';
import 'package:app/globals.dart' as global;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _researchController = TextEditingController();
  bool _visible = true;
  Future _search;

  @override
  initState() {
    _search = getGallerySearch("top", "all", 1, "", global.accessToken);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _visible = !_visible;
            });
          },
          child: Icon(Icons.flip),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _researchController,
                onChanged: (text) {
                  setState(() {
                    _search = getGallerySearch(
                        "top", "all", 1, text, global.accessToken);
                  });
                },
                decoration: InputDecoration(
                    hintText: "Enter a search text",
                    prefixIcon: Icon(Icons.search),
                    suffixIcon: IconButton(
                        onPressed: () => _researchController.clear(),
                        icon: Icon(Icons.clear))),
              ),
            ),
            Expanded(
              child: FutureBuilder<PostGallerySearch>(
                future: _search,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.data.length < 1)
                      return Center(
                          child:
                              Text("Found ${snapshot.data.data.length} images found."));
                    else {
                      return ListView.builder(
                        itemCount: snapshot.data.data.length,
                        itemBuilder: (context, index) {
                          String fileName =
                              snapshot.data.data[index].link.split('.').last;
                          if (fileName.length > 4) {
                            return Container();
                          }
                          return FlippedImage(
                            link: snapshot.data.data[index].link,
                            opacity: _visible ? 1.0 : 0.0,
                          );
                        },
                      );
                    }
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
          ],
        ));
  }
}
