import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class PostGallerySearch {
  List<DataGallerySearch> data;
  bool success;
  int status;

  PostGallerySearch({
    this.data,
    this.success: false,
    this.status,
  });

  factory PostGallerySearch.fromJson(Map<String, dynamic> json) =>
      PostGallerySearch(
        data: List<DataGallerySearch>.from(
            json["data"].map((x) => DataGallerySearch.fromJson(x))),
        success: json["success"],
        status: json["status"],
      );
}

class DataGallerySearch {
  String id;
  String link;

  DataGallerySearch({
    this.id,
    this.link,
  });

  factory DataGallerySearch.fromJson(Map<String, dynamic> json) =>
      DataGallerySearch(
        id: json["id"],
        link: json["link"],
      );
}

Future<PostGallerySearch> getGallerySearch(String sort, String window, int page,
    String search, String accessToken) async {
  print("getGallerySearch");
  PostGallerySearch error;
  final res = await http.get(
    "https://api.imgur.com/3/gallery/search/" +
        sort +
        "/" +
        window +
        "/" +
        page.toString() +
        "?q_any=" +
        search,
    headers: {HttpHeaders.authorizationHeader: "Bearer " + accessToken},
  );
  if (res.statusCode == 200) {
    return PostGallerySearch.fromJson(json.decode(res.body));
  } else if (res.statusCode == 429) {
    print("getGallerySearch : Error 429");
    return error;
  }
  return error;
}
